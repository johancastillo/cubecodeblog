# Módulos
from django.db import models
from ckeditor.fields import RichTextField

# Modelo Base
class ModeloBase(models.Model):
    id = models.AutoField(primary_key = True)
    estado = models.BooleanField('Estado', default = True)
    fecha_creacion = models.DateField('Fecha de creación', auto_now = False, auto_now_add = True)
    fecha_modificacion = models.DateField('Fecha de modificación', auto_now = True, auto_now_add = False)
    fecha_eliminacion = models.DateField('Fecha de eliminación', auto_now = True, auto_now_add = False)

    class Meta:
        abstract = True

# Modelo sobre las categorías de los posts
class Categoria(ModeloBase):
    nombre = models.CharField('Nombre de la categoría', max_length = 100, unique = True)
    imagen_referencial = models.ImageField('Imágen referencial', upload_to = 'categoria/')

    class Meta:
        verbose_name = 'Categoría'
        verbose_name_plural = 'Categorías'

    def __str__(self):
        return self.nombre

# Modelo sobre la información del autor
class Autor(ModeloBase):
    nombre = models.CharField('Nombre', max_length = 100)
    apellidos = models.CharField('Apellidos', max_length = 120)
    email = models.EmailField('Correo electrónico', max_length = 200)
    descripcion = models.TextField('Descripción')
    web = models.URLField('Web', null = True, blank = True)
    facebook = models.URLField('Facebook', null = True, blank = True)
    instagram = models.URLField('Instagram', null = True, blank = True)
    twitter = models.URLField('Twitter', null = True, blank = True)

    class Meta:
        verbose_name = 'Autor'
        verbose_name_plural = 'Autores'

    def __str__(self):
        return '{0},{1}'.format(self.nombre, self.apellidos)

# Modelo sobre el Post
class Post(ModeloBase):
    titulo = models.CharField('Título del post', max_length = 150, unique = True)
    slug = models.CharField('Slug', max_length = 150, unique = True)
    descripcion = models.TextField('Descripción')
    autor = models.ForeignKey(Autor, on_delete = models.CASCADE)
    categoria = models.ForeignKey(Categoria, on_delete = models.CASCADE)
    imagen_referencial= models.ImageField('Imagen referencial', upload_to = 'imagenes/', max_length = 255)
    publicado = models.BooleanField('Publicado / No publicado', default = False)
    fecha_publicacion = models.DateField('Fecha de publicación')
    contenido = RichTextField()

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'

    def __str__(self):
        return self.titulo

# Modelo sobre la información del sitio web en sí
class Web(ModeloBase):
    nosotros = models.TextField('Nosotros')
    telfono = models.CharField('Telefono', max_length = 12)
    email = models.EmailField('Correo electrónico', max_length = 200)
    direccion = models.CharField('Dirección', max_length = 200)

    class Meta:
        verbose_name = 'Web'
        verbose_name_plural = 'Webs'

    def __str__(self):
        return self.nosotros

#Modelo para las redes sociales
class RedesSociales(ModeloBase):
    facebook = models.URLField('Facebook')
    twiter = models.URLField('Twitter')
    instagram = models.URLField('Instagram')

    class Meta:
        verbose_name = 'Red Social'
        verbose_name_plural = 'Redes Sociales'

    def __str__(self):
        return self.instagram

#Modelo sobre la información de contacto
class Contacto(ModeloBase):
    nombre = models.CharField('Nombre', max_length = 100)
    apellidos = models.CharField('Epellidos', max_length = 150)
    email = models.EmailField('Correo electrónico', max_length = 200)
    asunto = models.CharField('Asunto', max_length = 100)
    mensaje = models.TextField('Mensaje')

    class Meta:
        verbose_name = 'Contacto'
        verbose_name_plural = 'Contactos'

    def __str__(self):
        return self.asunto

# Modelo para las suscripciones del blog
class Suscriptor(ModeloBase):
    email = models.EmailField('Correo electrónico', max_length = 200)

    class Meta:
        verbose_name = 'Suscriptor'
        verbose_name_plural = 'Suscrptores'

    def __str__(self):
        return self.email
